import React from "react";
import unsplash from "../Api/unsplash";
import SearchBar from "./SearchBar";
import ImageList from "./ImageList";

class App extends React.Component {
  state = {
    images: [],
  };
  // first method
  //   onSearchSubmit(terms) {
  //     axios
  //       .get("https://api.unsplash.com/search/photos", {
  //         params: {
  //           query: terms,
  //         },
  //         headers: {
  //           Authorization:
  //             "Client-ID BdHdFfNQoy5THgQeOLvk_eG4bdX3HW8WFgzOvpuZx9g",
  //         },
  //       })
  //       .then((response) => {
  //         console.log(response.data.results);
  //       });
  //   }

  // second method
  onSearchSubmit = async (terms) => {
    const response = await unsplash.get("/search/photos", {
      params: {
        query: terms,
      },
    });
    this.setState({ images: response.data.results });
  };

  render() {
    return (
      <div className="ui container" style={{ marginTop: "10px" }}>
        <SearchBar onSubmit={this.onSearchSubmit}></SearchBar>
        <ImageList images={this.state.images}></ImageList>
      </div>
    );
  }
}
export default App;
