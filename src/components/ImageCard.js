import React, { Component } from "react";

class ImageCard extends Component {
  constructor(props) {
    super(props);
    this.state = { spans: 0 };
    this.ImageRefs = React.createRef();
  }

  componentDidMount() {
    this.ImageRefs.current.addEventListener("load", this.setSpan);
  }

  setSpan = () => {
    const height = this.ImageRefs.current.clientHeight;
    const spans = Math.ceil(height / 10);
    this.setState({ spans });
  };
  render() {
    const { urls, description } = this.props.image;
    return (
      <div style={{ gridRowEnd: `span ${this.state.spans}` }}>
        <img ref={this.ImageRefs} src={urls.regular} alt={description}></img>
      </div>
    );
  }
}

export default ImageCard;
