import React from "react";

class SearchBar extends React.Component {
  //   constructor() {
  //     super();
  //     this.OnFormSubmit = this.OnFormSubmit.bind(this);
  //   }
  state = {
    terms: "",
  };
  OnFormSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state.terms);
  };
  render() {
    return (
      <div className="ui segment">
        <form onSubmit={this.OnFormSubmit} className="ui form">
          <div className="field">
            <label>Image Search:</label>
            <input
              type="text"
              value={this.state.terms}
              onChange={(e) => this.setState({ terms: e.target.value })}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
